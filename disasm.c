#include <stdio.h>
#include <stdlib.h>

#include "common.h"

static int disasm(FILE *f);

static int
disasm(FILE *const f)
{
	unsigned short op;
	unsigned char bts[2];
	while (1) {
		if (fread(&bts[0], sizeof(unsigned char), 1, f) < 1)
			break;
		if (fread(&bts[1], sizeof(unsigned char), 1, f) < 1)
			break;
		op = (((bts[0] & 0xFFU) << 8U) & 0xFF00U) | (bts[1] & 0xFFU);
		switch (OPA(op)) {
		case 0x0:
			switch (OPKK(op)) {
			case 0xE0:
				printf("%-5s\n", SYMCLS);
				break;
			case 0xEE:
				printf("%-5s\n", SYMRET);
				break;
			default:
				printf("%-5s %u\n", SYMSYS, OPNNN(op));
				break;
			}
			break;
		case 0x1:
			printf("%-5s %u\n", SYMJP, OPNNN(op));
			break;
		case 0x2:
			printf("%-5s %u\n", SYMCALL, OPNNN(op));
			break;
		case 0x3:
			printf("%-5s %s%1.1X, %u\n", SYMSE, SYMV, OPX(op), OPKK(op));
			break;
		case 0x4:
			printf("%-5s %s%1.1X, %u\n", SYMSNE, SYMV, OPX(op), OPKK(op));
			break;
		case 0x5:
			if (OPN(op) != 0x0)
				goto failure;
			printf("%-5s %s%1.1X, %s%1.1X\n", SYMSE, SYMV, OPX(op), SYMV, OPY(op));
			break;
		case 0x6:
			printf("%-5s %s%1.1X, %u\n", SYMLD, SYMV, OPX(op), OPKK(op));
			break;
		case 0x7:
			printf("%-5s %s%1.1X, %u\n", SYMADD, SYMV, OPX(op), OPKK(op));
			break;
		case 0x8:
			switch (OPN(op)) {
			case 0x0:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMLD, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x1:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMOR, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x2:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMAND, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x3:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMXOR, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x4:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMADD, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x5:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMSUB, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0x6:
				printf("%-5s %s%1.1X\n", SYMSHR, SYMV, OPX(op));
				break;
			case 0x7:
				printf("%-5s %s%1.1X, %s%1.1X\n", SYMSUBN, SYMV, OPX(op), SYMV, OPY(op));
				break;
			case 0xE:
				printf("%-5s %s%1.1X\n", SYMSHL, SYMV, OPX(op));
				break;
			default:
				goto failure;
			}
			break;
		case 0x9:
			if (OPN(op) != 0x0)
				goto failure;
			printf("%-5s %s%1.1X, %s%1.1X\n", SYMSNE, SYMV, OPX(op), SYMV, OPY(op));
			break;
		case 0xA:
			printf("%-5s %s, %u\n", SYMLD, SYMI, OPNNN(op));
			break;
		case 0xB:
			printf("%-5s %s0, %u\n", SYMJP, SYMV, OPNNN(op));
			break;
		case 0xC:
			printf("%-5s %s%1.1X, %u\n", SYMRND, SYMV, OPX(op), OPKK(op));
			break;
		case 0xD:
			printf("%-5s %s%1.1X, %s%1.1X, %u\n", SYMDRW, SYMV, OPX(op), SYMV, OPY(op), OPN(op));
			break;
		case 0xE:
			switch (OPKK(op)) {
			case 0x9E:
				printf("%-5s %s%1.1X\n", SYMSKP, SYMV, OPX(op));
				break;
			case 0xA1:
				printf("%-5s %s%1.1X\n", SYMSKNP, SYMV, OPX(op));
				break;
			default:
				goto failure;
			}
			break;
		case 0xF:
			switch (OPKK(op)) {
			case 0x07:
				printf("%-5s %s%1.1X, %s\n", SYMLD, SYMV, OPX(op), SYMDT);
				break;
			case 0x0A:
				printf("%-5s %s%1.1X, %s\n", SYMLD, SYMV, OPX(op), SYMKEY);
				break;
			case 0x15:
				printf("%-5s %s, %s%1.1X\n", SYMLD, SYMDT, SYMV, OPX(op));
				break;
			case 0x18:
				printf("%-5s %s, %s%1.1X\n", SYMLD, SYMST, SYMV, OPX(op));
				break;
			case 0x1E:
				printf("%-5s %s, %s%1.1X\n", SYMADD, SYMI, SYMV, OPX(op));
				break;
			case 0x29:
				printf("%-5s %s, %s%1.1X\n", SYMLD, SYMFNT, SYMV, OPX(op));
				break;
			case 0x33:
				printf("%-5s %s, %s%1.1X\n", SYMLD, SYMBCD, SYMV, OPX(op));
				break;
			case 0x55:
				printf("%-5s %s, %s%1.1X\n", SYMLD, SYMIM, SYMV, OPX(op));
				break;
			case 0x65:
				printf("%-5s %s%1.1X, %s\n", SYMLD, SYMV, OPX(op), SYMIM);
				break;
			default:
				goto failure;
			}
			break;
		}
	}
	return 0;
failure:
	printe("Invalid instruction 0x%4.4x encountered.", op);
	return -1;
}

int
main(int argc, char *argv[])
{
	FILE *f;
	int rc;
	if (argc != 2) {
		printf("Usage: ./disasm.x [file]\n\n");
		printf("\tfile - The file to disassemble.\n");
		return EXIT_SUCCESS;
	}
	if ((f = fopen(argv[1], "rb")) == NULL) {
		printe("Unable to open file %s.", argv[1]);
		return EXIT_FAILURE;
	}
	rc = EXIT_SUCCESS;
	if (disasm(f) < 0) {
		rc = EXIT_FAILURE;
		goto end;
	}
	if (ferror(f) != 0) {
		printe("Failed to read from file %s.", argv[1]);
		rc = EXIT_FAILURE;
	}
end:
	fclose(f);
	return rc;
}
